package br.com.teste.consumer;

import br.com.teste.producer.Producer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class Consumer {
    private final Logger logger = LoggerFactory.getLogger(Consumer.class);

    private Producer producer;

    @Autowired
    Consumer(Producer producer, Environment env){
        this.producer = producer;
    }



    @KafkaListener(topics = "${topic.name}")
    public void listen(ConsumerRecord<String, String> payload){
        logger.info(String.format("Mensagem: ", payload.value(), payload.offset(), payload.partition()));

        this.producer.sendMessage("topic-avro-teste");

    }
}
