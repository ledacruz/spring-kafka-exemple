package br.com.teste.producer;

import br.com.teste.avro.Cliente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {

    private static final Logger logger = LoggerFactory.getLogger(Producer.class);

    @Autowired
    private KafkaTemplate<String, Cliente> kafkaTemplate;

    public void sendMessage(String topic){
        Cliente cliente = Cliente.newBuilder()
                .setClienteId("01")
                .setNome("Leda")
                .build();

        logger.info(String.format("Produzindo mensagem", cliente.toString()));
        kafkaTemplate.send(topic, cliente);

    }
}