package br.com.teste.br.com.teste.br.com.teste.controller;

import br.com.teste.producer.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= "/kafka")
public class Controller {
    private Producer producer;

    @Autowired
    Controller(Producer producer, Environment env){
        this.producer = producer;
    }

    @PostMapping(value = "/publish")
    public void sendMessageToKafkaTopic(){
        this.producer.sendMessage("topic-avro-teste");
    }

}
